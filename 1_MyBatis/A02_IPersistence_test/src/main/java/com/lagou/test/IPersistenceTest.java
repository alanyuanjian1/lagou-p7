package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class IPersistenceTest {
    @Test
    public void test1() throws Exception {
        //（1）加载配置
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");

        // (2) 创建工厂
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);

        // (3) 打开会话
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // (4) 查询条件
        User user = new User();
        user.setId(2);
        user.setUsername("tom");

        // (5) 查询
        User user2 = sqlSession.selectOne("User.selectOne", user);
        System.out.println(user2);

        // (6) 查询
        List<User> users = sqlSession.selectList("User.selectList");

        for (User user1 : users) {
            System.out.println(user1);
        }
    }

    @Test
    public void test2() throws Exception {
        //（1）加载配置
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        // (2) 创建工厂
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        // (3) 打开会话
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // （4）查询条件
        User user = new User();
        user.setId(1);
        user.setUsername("张三");

        // (5) 执行
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);

        List<User> all = userDao.findAll();

        for (User user1 : all) {
            System.out.println(user1);
        }
    }
}
